<?php
// simply return the list of possible usernames
  $str = file_get_contents('json_list.json');
  $json = json_decode($str, true);
  $output =  '
{"usernames":[
  ';
  foreach($json['usernames'] as $row) {
	foreach($row as $key=>$val){
      $output .= "\"$key\",";
	}
  }
 echo rtrim($output, ',').'
  ]
}
';
?>
